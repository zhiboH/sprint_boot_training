package com.example.lab2;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.LocalDateTime;

@RestController
public class TimeStampController {

    @RequestMapping("/date")
    public String getDate(){
        return "Current Date " + LocalDate.now().toString();
    }

    @RequestMapping("/Time")
    public String getTime(){
        return "Current Time " +LocalDateTime.now().toString();
    }
}
