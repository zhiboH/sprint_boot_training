package student.onlineretailer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.Map;

@Component
public class CartServiceImpl implements CartService {
    @Autowired
    private CartRepository repository;

    @Value("#{catalog}")
    private Map<Integer,Item>catalog;

    @Value("${contactEmail}")
    private String contactEmaill;

    @Value("${onlineRetailer.salesTaxRate}")
    private double salesTaxRate;

    @Value("${onlineRetailer.deliveryCharge.normal}")
    private double normalDelivery;

    @Value("${onlineRetailer.deliveryCharge.threshold}")
    private double thresholdDeliver;


    @Override
    public void addItemToCart(int id, int quantity){
        if (catalog.containsKey(id)){
            repository.add(id, quantity);
        }
    }
    public void removeItemFromCart(int id){
        repository.remove(id);

    }
    public Map<Integer, Integer> getAllItemsInCart(){
        return  repository.getAll();
    }
    public double calculateCartCost(){
        Map<Integer,Integer> items =repository.getAll();
        double totalCost =0;
        for(Integer id: items.keySet()){
            int quantity = items.get(id);
            Item item= catalog.get(id);
            double price= item.getPrice();
            totalCost=totalCost+(quantity*price);
        }
        return totalCost;

    }
    public double totalAfterTax(){
        double totalCost=calculateCartCost();
        return  totalCost*(1+salesTaxRate);
    }
    public double totalAfterDelivery(){
        double totalCost= calculateCartCost();
        if (totalCost==0 ||totalCost >thresholdDeliver){
            return totalAfterTax();
        }
        else {
            return totalAfterTax()+normalDelivery;
        }


    }
}
