package student.onlineretailer;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "resources")
public class ResourcesBean {
    private String db;
    private String log;

    public String getLog() {
        return log;
    }
    public String getDb(){
        return db;
    }
    public void setDb(String db) {
        this.db = db;
    }

    public void setLog(String log) {
        this.log = log;
    }

    @Override
    public String toString() {
        return "The db is " +db +", The log is "+log;
    }
}
