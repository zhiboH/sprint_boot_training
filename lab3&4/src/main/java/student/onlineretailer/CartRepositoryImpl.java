package student.onlineretailer;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class CartRepositoryImpl implements CartRepository {
    private Map<Integer, Integer> shopping_Cart= new HashMap<Integer, Integer>();

    public void add(int itemId, int quantity){
        shopping_Cart.put(itemId,quantity);
    }
    public void remove(int itemId){
        shopping_Cart.remove(itemId);
    }
    public Map getAll(){
        return shopping_Cart;
    }
}
